/** @type {import('tailwindcss').Config} */

import daisyui from 'daisyui';
export default {
  content: ['./src/**/*.tsx'],
  theme: {
    extend: {
      transitionProperty: {
        'width': 'width'
      }
    }
  },
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#413C58",
          "secondary": "#835DF0",
          "neutral": "#3D4451",
          "base-100": "#FFFFFF",
          "info": "#4273D2",
          "success": "#55917F",
          "warning": '#F0E45D',
          "error": "#F0775D",
        },
      },
    ],
  },
  plugins: [daisyui]
}

