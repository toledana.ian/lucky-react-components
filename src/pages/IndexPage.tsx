import React, { useEffect, useState } from 'react';
import { BlankLayoutComponent } from '../components/BlankLayoutComponent';
import { PlayersCardComponent } from '../components/PlayersCard';

export const IndexPage = () => {
	const [odds, setOdds] = useState(3.5);
	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		setIsLoading(false);
	}, [odds]);

	return (
		<>
			<BlankLayoutComponent>
				<PlayersCardComponent
					playerId={1}
					playerNumber={1}
					playerName={'Juan Dela Cruz'}
					currentScore={43}
					handicap={2}
					initialTimer={5}
					odds={odds}
					totalBet={1000000}
					betPlaced={1500}
					isBetButtonLoading={isLoading}
					onClickBet={(playerId) => {
						console.log(playerId);
						setOdds((prevState) => prevState + 1);
						setIsLoading((prevState) => !prevState);
					}}
				/>
			</BlankLayoutComponent>
		</>
	);
};
