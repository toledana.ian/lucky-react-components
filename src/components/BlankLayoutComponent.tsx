import React, { FunctionComponent, ReactNode } from 'react';

export const BlankLayoutComponent: FunctionComponent<{
	children: ReactNode;
}> = ({ children }) => {
	return (
		<>
			<div className={'bg-image-layout-gradient min-h-screen'}>{children}</div>
		</>
	);
};
