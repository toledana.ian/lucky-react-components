import { useEffect, useState } from 'react';
import { formatPesoNumber } from '../helper/StringHelper';

const formatTime = (time: number) => {
	const minutes: number = Math.floor(time / 60);
	const seconds: number = Math.floor(time - minutes * 60);
	let strMinutes = minutes + '';
	let strSeconds = seconds + '';

	if (minutes <= 10) strMinutes = '0' + strMinutes;
	if (seconds <= 10) strSeconds = '0' + strSeconds;

	return strMinutes + ':' + strSeconds;
};

interface IPlayersCardComponentProps {
	playerId: number;
	playerNumber: number;
	playerName: string;
	currentScore: number;
	handicap: number;
	initialTimer: number;
	odds: number;
	totalBet: number;
	betPlaced: number;
	isBetButtonLoading: boolean;
	onClickBet: (playerId: number) => void;
}

export const PlayersCardComponent = (props: IPlayersCardComponentProps) => {
	const {
		playerId,
		playerNumber,
		playerName,
		currentScore,
		handicap,
		initialTimer,
		odds,
		totalBet,
		betPlaced,
		isBetButtonLoading,
		onClickBet
	} = props;

	const [countdown, setCountDown] = useState(initialTimer);

	useEffect(() => {
		const timer = setInterval(() => {
			setCountDown((prev) => (prev === 0 ? initialTimer : prev - 1));
		}, 1000);
		return () => clearInterval(timer);
	}, [initialTimer]);

	useEffect(() => {
		setCountDown(initialTimer);
	}, [initialTimer, odds]);

	return (
		<>
			<div className={'mx-auto max-w-sm'}>
				{
					<div
						className="font-kallisto-bold py-[11px] uppercase"
						id={`${playerId}`}
					>
						<div className="padding card gap-0 rounded-[5px] bg-white p-[10px]">
							<div className="card-body gap-0 p-0">
								<div className="flex w-full flex-row items-center justify-between">
									<div className="bg-violet flex h-[21.43px] min-w-[21.43px]  justify-center  rounded-full text-[16px] leading-[22px] text-white">
										{playerNumber}
									</div>
									<h5 className="mr-auto px-[10px] text-[16px] leading-[18px]">
										{playerName}
									</h5>

									<div className="flex flex-row gap-[6px]">
										<div className="flex h-[70px] w-[70px] rounded-[5px] border-[1px] border-solid border-slate-300 text-center">
											<div className="m-auto leading-[21px]">
												<span className="inline-block w-[53px] text-[8px] leading-[9px]">
													Start Score
												</span>
												<div className="current-score text-[23px] leading-[23px]">
													{currentScore}
												</div>
											</div>
										</div>

										{handicap !== 0 ? (
											<div className="bg-theme-error flex h-[70px] w-[70px] rounded-[5px] border-[1px] border-solid text-center text-white">
												<div className="m-auto mt-[8px] leading-[21px]">
													<span className="inline-block w-[53px] text-[8px] leading-[9px]">
														Handicap
													</span>
													<div className="current-score text-[23px] leading-[23px]">
														+{handicap}
													</div>
												</div>
											</div>
										) : (
											<div className="flex h-[70px] w-[70px] rounded-[5px] border-[1px] border-solid border-slate-300 text-center">
												<div className="m-auto mt-[8px] leading-[21px] text-[#9D9D9D]">
													<span className="inline-block w-[53px] text-[8px] leading-[9px]">
														Handicap
													</span>
													<div className="current-score text-[23px] leading-[23px]">
														0
													</div>
												</div>
											</div>
										)}
									</div>
								</div>

								<div className="total-bets mt-[10px] flex h-[30px] items-center justify-center bg-gray-400 text-[12px]">
									TOTAL BETS : {formatPesoNumber(totalBet)}
								</div>

								<ul className="flex w-full flex-row items-center justify-around">
									<li className="w-full border-r-[1px] border-slate-300 pb-[8px] text-center">
										<span className="text-[9px]">
											Odds ({formatTime(countdown)})
										</span>
										<div className="text-[15px] leading-[15px]">{odds}x</div>
									</li>

									<li className="w-full border-r-[1px] border-slate-300 pb-[8px] text-center">
										<span className="text-[9px]">Placed Bets</span>
										<div className="text-[15px] leading-[15px]">
											{formatPesoNumber(betPlaced)}
										</div>
									</li>

									<li className="w-full pb-[8px] text-center">
										<span className="text-[9px]">Payout</span>
										<div className="text-[15px] leading-[15px]">
											{formatPesoNumber(odds * betPlaced)}
										</div>
									</li>
								</ul>

								<button
									type="button"
									className="text-[16px]outline-none bg-energy-yellow-700 font-kallisto-bold mt-[6px] h-[40px] rounded-[5px] px-6 py-1 uppercase"
									onClick={() => {
										onClickBet(playerId);
									}}
								>
									{isBetButtonLoading ? 'loading...' : 'Bet'}
								</button>
							</div>
						</div>
					</div>
				}
			</div>
		</>
	);
};
