export const formatPesoNumber = (number: number) => {
	return Number(number).toLocaleString('en-US', {
		style: 'currency',
		currency: 'PHP',
		minimumFractionDigits: 2
	});
};

export const formatLeadingZero = (num: number, places: number) => {
	const zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join('0') + num;
};
