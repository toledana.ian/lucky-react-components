export const formatDateShortWord = (date: Date) => {
	return date.toLocaleDateString('en-US', {
		year: 'numeric',
		month: 'short',
		day: 'numeric'
	});
};

export const formatDateNumberOnly = (date: Date) => {
	const month = (date.getMonth() + 1).toString().padStart(2, '0');
	const day = date.getDate().toString().padStart(2, '0');
	const year = date.getFullYear();

	return `${month}/${day}/${year}`;
};

export const formatHour = (date: Date) => {
	const hours = date.getHours();
	const am_pm = hours < 12 ? 'AM' : 'PM';
	const hour = hours % 12 || 12;

	return hour + ' ' + am_pm;
};

export const formatTime = (date: Date): string => {
	const hours = date.getHours() % 12 || 12;
	const minutes = date.getMinutes().toString().padStart(2, '0');
	const ampm = date.getHours() >= 12 ? 'pm' : 'am';
	return `${hours}:${minutes} ${ampm}`;
};

export const formatLongDateTime = (date: Date): string => {
	return date.toLocaleString('en-US', {
		month: 'numeric',
		day: 'numeric',
		year: 'numeric',
		hour: 'numeric',
		minute: 'numeric',
		second: 'numeric',
		hour12: true
	});
};
