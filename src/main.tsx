import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import './style.css'
import { IndexPage } from './pages/IndexPage';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <IndexPage />
)
